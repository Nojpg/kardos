package builder;

import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;

class InnerBuilder<T> {

    private T instance;

    InnerBuilder(Class<T> tClass) {
        try {
            instance = tClass.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    void set(Consumer<T> setter) {
        setter.accept(instance);
    }

    T build() {
        return instance;
    }
}
