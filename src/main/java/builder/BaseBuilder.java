package builder;

import generic.GenericInterface0;
import throwable.ThrowingConsumer;

import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public abstract class BaseBuilder<T, R extends BaseBuilder<T, R>> implements GenericInterface0 {

    private InnerBuilder<T> builder;

    public BaseBuilder() {
        this.builder = new InnerBuilder<T>(getType0());
    }

    public T build() {
        return builder.build();
    }

    protected R set(Consumer<T> consumer) {
        builder.set(ThrowingConsumer.unchecked(consumer::accept));
        return (R) this;
    }
}
