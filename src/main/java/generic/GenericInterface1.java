package generic;

import throwable.ThrowingSupplier;

import java.lang.reflect.ParameterizedType;

@SuppressWarnings("unchecked")
public interface GenericInterface1<T, N> extends GenericInterface0<T>{
    default Class<N> getType1() {
        String typeName = ((ParameterizedType) getClass().getGenericInterfaces()[0]).getActualTypeArguments()[1].getTypeName();
        return ThrowingSupplier.unchecked(() -> (Class<N>)Class.forName(typeName)).get();
    }
}
