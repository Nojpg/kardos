package generic;

import throwable.ThrowingSupplier;

import java.lang.reflect.ParameterizedType;

@SuppressWarnings("unchecked")
public interface GenericInterface2<T, N, R> extends GenericInterface1<T, N> {
    default Class<R> getType2() {
        String typeName = ((ParameterizedType) getClass().getGenericInterfaces()[0]).getActualTypeArguments()[2].getTypeName();
        return ThrowingSupplier.unchecked(() -> (Class<R>)Class.forName(typeName)).get();
    }
}
