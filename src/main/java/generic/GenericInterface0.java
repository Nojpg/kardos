package generic;

import throwable.ThrowingSupplier;

import java.lang.reflect.ParameterizedType;

@SuppressWarnings("unchecked")
public interface GenericInterface0<T> {
    default Class<T> getType0() {
        String typeName = getClass().getGenericSuperclass() == Object.class
                ? ((ParameterizedType) getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0].getTypeName()
                : ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
        return ThrowingSupplier.unchecked(() -> (Class<T>)Class.forName(typeName)).get();
    }
}
